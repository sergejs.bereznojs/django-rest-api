from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from ingredients.models import Ingredient
from ingredients.serializers import IngredientSerializer


@api_view(['GET', 'POST'])
def ingredient_list(request, format=None):
    """
    List all ingredient or add a new ingredient
    """
    if request.method == 'GET':
        ingredients = Ingredient.objects.all()
        serializer = IngredientSerializer(ingredients, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = IngredientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def ingredient_detail(request, name, format=None):
    """
    Get, update, or delete an ingredient object
    """
    try:
        ingredient = Ingredient.objects.get(name=name)
    except Ingredient.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = IngredientSerializer(ingredient)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = IngredientSerializer(ingredient, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        ingredient.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def ingredient_weight(request, name, weight):
    """
    Get ingredient weight after performing different processing methods.
    Need to provide ingredient name and gross weight
    """
    try:
        ingredient = Ingredient.objects.get(name=name)
    except Ingredient.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    weight_results = [str(i) for i in ingredient.get_weight(weight)]

    result = {
        'name': ingredient.name,
        'gross_weight': weight,
        'weight_after_initial_processing': weight_results[0],
        'weight_after_boiling': weight_results[1],
        'weight_after_frying': weight_results[2]
    }

    return Response(result)
