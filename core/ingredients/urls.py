from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from ingredients import views

urlpatterns = [
    url(r'^ingredients/$', views.ingredient_list),
    url(r'^ingredients/(?P<name>[a-z]+)/$', views.ingredient_detail),
    url(r'^ingredients/(?P<name>[a-z]+)/(?P<weight>[0-9]+)/$', views.ingredient_weight),
]

urlpatterns = format_suffix_patterns(urlpatterns)
