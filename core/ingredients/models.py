from django.db import models
from decimal import Decimal


class Ingredient(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, unique=True)
    food_type = models.CharField(max_length=100, blank=True, default='')
    initial_processing_losses = models.DecimalField(max_digits=5, decimal_places=2)
    boiling_losses = models.DecimalField(max_digits=5, decimal_places=2)
    frying_losses = models.DecimalField(max_digits=5, decimal_places=2)

    class Meta:
        ordering = ('food_type',)

    def __str__(self):
        return self.name

    def get_weight(self, gross_weight):
        gross_weight = int(gross_weight)
        after_processing = gross_weight - (gross_weight * Decimal(self.initial_processing_losses))
        after_boiling = after_processing - (after_processing * Decimal(self.boiling_losses))
        after_frying = after_processing - (after_processing * Decimal(self.frying_losses))
        return [int(round(after_processing)), int(round(after_boiling)), int(round(after_frying))]
