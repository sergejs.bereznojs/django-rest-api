from django.test import TestCase
from ingredients.models import Ingredient


class IngredientTest(TestCase):

    def setUp(self):
        self.ingredient = Ingredient.objects.create(
            name='potato',
            initial_processing_losses='0.25',
            boiling_losses='0.03',
            frying_losses='0.31',
        )

    def test_create_object(self):
        self.assertEqual(self.ingredient.name, 'potato')
        self.assertEqual(self.ingredient.initial_processing_losses, '0.25')
        self.assertEqual(self.ingredient.boiling_losses, '0.03')
        self.assertEqual(self.ingredient.frying_losses, '0.31')

    def test_weight_calculations(self):
        self.assertEqual(self.ingredient.get_weight(100)[0], 75)
        self.assertEqual(self.ingredient.get_weight(100)[1], 73)
        self.assertEqual(self.ingredient.get_weight(100)[2], 52)
