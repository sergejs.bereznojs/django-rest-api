from rest_framework import serializers
from ingredients.models import Ingredient


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('id', 'name', 'initial_processing_losses', 'boiling_losses', 'frying_losses', 'food_type')
