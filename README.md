# A simple API based on Django and Django REST frameworks

### Description

This simple API allows a user to access a database of cooking ingredients and determine weight losses after performing different ingredient processing steps.
 

### Local project setup

In order to run the project locally, please follow these steps:

- Clone the project: `git clone git@gitlab.com:sergejs.bereznojs/django-rest-api.git`
- Set up virtual environment: `python3.6 -m venv env`
- Activate virtual environment: `source env/bin/activate`
- Install the requirements: `pip install -r requirements.txt`
- Perform migrations: `python manage.py migrate`
- Load test data: `python manage.py loaddata fixtures.json`
- Run test server: `python manage.py runserver`
- Server is ready to process requests

### API Documentation

#### Get the list of all available ingredients

**Request**
```python
GET /ingredients/
```
**Response**
```python
HTTP 200 OK
Allow: OPTIONS, POST, GET
Content-Type: application/json
Vary: Accept

[
    {
        "id": 1,
        "name": "potato",
        "initial_processing_losses": "0.25",
        "boiling_losses": "0.03",
        "frying_losses": "0.31"
    },
    {
        "id": 2,
        "name": "cabbage",
        "initial_processing_losses": "0.20",
        "boiling_losses": "0.08",
        "frying_losses": "0.25"
    }
]
```

#### Add new ingredient to the database

**Request**

```python
POST /ingredients/
{
   "name": "portobello",
    "initial_processing_losses": "0.24",
    "boiling_losses": "0.25",
    "frying_losses": "0.6"
}
```

**Response**
```python
HTTP 201 Created
Allow: OPTIONS, POST, GET
Content-Type: application/json
Vary: Accept

{
    "id": 4,
    "name": "portobello",
    "initial_processing_losses": "0.24",
    "boiling_losses": "0.25",
    "frying_losses": "0.60"
}
```
#### Get details for a particular ingredient 

**Request**

```python
GET /ingredients/<ingredient_name>/
```

**Response**

```python
HTTP 200 OK
Allow: OPTIONS, PUT, DELETE, GET
Content-Type: application/json
Vary: Accept

{
    "id": 1,
    "name": "potato",
    "initial_processing_losses": "0.25",
    "boiling_losses": "0.03",
    "frying_losses": "0.31"
}
```

#### Update details for a particular ingredient

**Request**

```python
PUT /ingredients/<ingredient_name>/
{
    "name": "portobello",
    "initial_processing_losses": "0.24",
    "boiling_losses": "0.25",
    "frying_losses": "0.60",
    "food_type": "mushrooms"
}
```

**Response**
```python
HTTP 200 OK
Allow: PUT, GET, DELETE, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "id": 4,
    "name": "portobello",
    "initial_processing_losses": "0.24",
    "boiling_losses": "0.25",
    "frying_losses": "0.60",
    "food_type": "mushrooms"
}
```

#### Delete a particular ingredient

**Request**

```python
DELETE /ingredients/<ingredient_name>/
```

**Response**
```python
HTTP 204 No Content
Allow: PUT, GET, DELETE, OPTIONS
Content-Type: application/json
Vary: Accept
```

#### Get ingredient weight after performing different processing methods

**Request**

```python
GET /ingredients/<ingredient_name>/<ingredient_weight>/
```

**Response**

```python
HTTP 200 OK
Allow: GET, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "name": "portobello",
    "gross_weight": "100",
    "weight_after_initial_processing": "76",
    "weight_after_boiling": "57",
    "weight_after_frying": "30"
}
```